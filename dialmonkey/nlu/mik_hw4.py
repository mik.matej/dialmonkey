import tensorflow as tf
import numpy as np
from ..component import Component
from ..da import DAI
from pathlib import Path

def to_bag(sequence, categories):
    bag = np.zeros(len(categories) + 1)
    for token in sequence:
        try:
            bag[categories.index(token)] += 1
        except ValueError:
            bag[len(categories)] += 1
    return bag

class NLU(Component):
    
    _words = open(Path(__file__).parents[2] / 'data/mik_hw4/words.txt').read().rstrip().split('\n')
    _DAs = open(Path(__file__).parents[2] / 'data/mik_hw4/DAs.txt').read().rstrip().split('\n')
    _model = tf.keras.models.load_model(str(Path(__file__).parents[2] / 'data/mik_hw4/NLU_model'))
        
    def __call__(self, dial, logger):
        x = np.zeros((1, len(self._words) + 1))
        x[0] = to_bag(dial.user.split(), self._words)
        predicted = self._model.predict(x)
        distr = predicted[0]
        for j, p in enumerate(distr):
            if p >= 0.5:
                try:
                    dial.nlu.append(DAI.parse(self._DAs[j]))
                except IndexError:
                    pass
        
        logger.info('NLU: %s', str(dial['nlu']))
        return dial